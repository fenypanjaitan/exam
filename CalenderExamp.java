package com.ns;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalenderExamp {
    public static void main(String[] args) {
        int sYear = 1998;
        int sMonth = 7;//agustus
//
//        Calendar calendar = new GregorianCalendar();
//        int cDay = calendar.get(Calendar.DATE);
//        int cMonth = calendar.get(Calendar.MONTH);
//        int cYear = calendar.get(Calendar.YEAR);

        GregorianCalendar gCal = new GregorianCalendar(sYear,sMonth,1);
        int days = gCal.getActualMaximum(Calendar.DATE);
        int startInWeek = gCal.get(Calendar.DAY_OF_WEEK);

        gCal = new GregorianCalendar(sYear,sMonth,days);
        int totalWeeks = gCal.getActualMaximum(Calendar.WEEK_OF_MONTH);



        System.out.println();
        System.out.println("================ Bulan "+sMonth+" Tahun "+sYear+" ==================");
        System.out.println("M\t\tS\t\tS\t\tR\t\tK\t\tJ\t\tS ");
        int count = 1;
        for (int i = 1; i <=totalWeeks ; i++) {
            System.out.println();
            for (int j=1;j<=7;j++ ){
                if(count<startInWeek || (count - startInWeek+1)>31){
                    System.out.print("_");
                    System.out.print("\t\t");
                } else {
                    System.out.print((count-startInWeek+1));
                    System.out.print("\t\t");
                }
                count++;
            }

        }
    }

//        GregorianCalendar gCal = new GregorianCalendar(cYear,cMonth,cDay);
//        int days = gCal.getActualMaximum(Calendar.DATE);
//        int startInWeek = gCal.get(Calendar.DAY_OF_WEEK);
//
//        gCal = new GregorianCalendar(cYear,cMonth,days);
//        int totalWeeks = gCal.getActualMaximum(Calendar.WEEK_OF_MONTH);
//
//        System.out.println();
//        System.out.println("================ Bulan "+cMonth+" Tahun "+cYear+" ==================");
//        System.out.println("M\t\tS\t\tS\t\tR\t\tK\t\tJ\t\tS ");
//        int count = 1;
//        for (int i = 1; i <=totalWeeks ; i++) {
//            System.out.println();
//            for (int j=1;j<=7;j++ ){
//                if(count<startInWeek || (count - startInWeek+1)>31){
//                    System.out.print("_");
//                    System.out.print("\t\t");
//                } else {
//                    System.out.print((count-startInWeek+1));
//                    System.out.print("\t\t");
//                }
//                count++;
//            }
//
//        }
//    }
}
