package com.ns;

public class MyCalendar {
    public void show(){
        int year = 2019;
        int firstDayYear = 1;


        for(int month = 1; month <= 12; month++)
        {
            int daysMonth = 0;

            String monthDisplay = "";

            switch(month)
            {
                case 1: monthDisplay = "January";
                    daysMonth = 31;
                    break;

                case 2:
                    monthDisplay = "February";
                    int leapYear = 0;
                    while (leapYear > -1)
                    {
                        leapYear += 4;

                        // If leap year, the days of the month will be 29.
                        if (year == leapYear)
                        {
                            daysMonth = 29;
                            break;
                        }

                        else
                        {
                            daysMonth = 28;
                        }
                    }
                    break;

                case 3: monthDisplay = "March";
                    daysMonth = 31;
                    break;

                case 4: monthDisplay = "April";
                    daysMonth = 30;
                    break;

                case 5: monthDisplay = "May";
                    daysMonth = 31;
                    break;

                case 6: monthDisplay = "June";
                    daysMonth = 30;
                    break;

                case 7: monthDisplay = "July";
                    daysMonth = 31;
                    break;

                case 8: monthDisplay = "August";
                    daysMonth = 31;
                    break;

                case 9: monthDisplay = "September";
                    daysMonth = 30;
                    break;

                case 10: monthDisplay = "October";
                    daysMonth = 31;
                    break;

                case 11: monthDisplay = "November";
                    daysMonth = 30;
                    break;

                case 12: monthDisplay = "December";
                    daysMonth = 31;
                    break;

                default : System.out.print("Invalid ");

            }
            // Display the month and year
            System.out.println();
            System.out.println("=================== "+ monthDisplay + " " + year+" =================== ");

            System.out.println("Sun\t\tMon\t\tTue\t\tWed\t\tThu\t\tFri\t\tSat");

            int firstDayEachMonth = (daysMonth + firstDayYear)%7;
            for (int space = 1; space <= firstDayEachMonth; space++)
                System.out.print("\t");

            for (int daysDisplay = 1; daysDisplay <= daysMonth; daysDisplay++)
            {
                if (firstDayYear%7 == 0)
                    System.out.println();

                System.out.printf("%3d\t\t", daysDisplay);
                firstDayYear += 1;
            }
            System.out.println();
        }

    }
}
